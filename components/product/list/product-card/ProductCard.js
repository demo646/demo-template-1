import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';

const useStyles = makeStyles({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  media: {
    height: 140,
  },
});

export default function ProductCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={props.image}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            {props.title.split(' ').slice(0, 3).join(' ')}
          </Typography>
          <Typography color="secondary" variant="body1" component="p">
            $ {props.price}
          </Typography>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Rating name="read-only" value={props.rating.rate} readOnly />{' '}
            <Typography
              component="span"
              variant="caption"
            >{` (${props.rating.count})`}</Typography>
          </div>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button fullWidth variant="outlined" size="small" color="primary">
          ADD TO CART
        </Button>
      </CardActions>
    </Card>
  );
}
