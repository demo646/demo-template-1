import { doRequest } from '../../utils/CoreUtils';

import { product_domain } from '../../constants/Domain';
import { errorHandler } from '../../utils/StringUtils';

export default class ProductApis {
  async getProducts() {
    try {
      let url = `${product_domain}products`;
      return await doRequest('get', url);
    } catch (error) {
      throw errorHandler(error);
    }
  }
}
