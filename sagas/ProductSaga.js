import { call, put } from 'redux-saga/effects';

import ProductAction from '../reduxs/ProductRedux';

import { getErrorMsg } from '../utils/StringUtils';

import ProductApis from '../services/APIs/ProductAPIs';

const api = new ProductApis();

export function* getProducts(action) {
  const { classify } = action;

  try {
    let resp = yield call(api.getProducts);

    yield put(ProductAction.getProductSuccess(classify, resp));
  } catch (error) {
    console.log('error', error);
    yield put(ProductAction.getProductFailure(classify, getErrorMsg(error)));
  }
}
