import {
  borderRadiuses,
  fontFamilys,
  colors,
  textColors,
} from '../../../assets/styles/Theme';

export const styles = (theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    overflow: 'auto',
    // display: 'flex',
    // justifyContent: 'center',
    // flexDirection: 'column',
  },
  container: {
    height: 'fit-content',
    maxWidth: 'calc(100% - 96px)',
    margin: 48,
    padding: '32px 48px',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'row',
    color: textColors.primary,
    fontFamily: fontFamilys.primary,
    backgroundColor: colors.white,
    borderRadius: borderRadiuses.primary,
    [theme.breakpoints.down('sm')]: {
      maxWidth: 'calc(100% - 60px)',
      margin: 30,
      padding: '12px 28px',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: 'calc(100% - 40px)',
      margin: 20,
      padding: '10px 26px',
    },
  },
});
