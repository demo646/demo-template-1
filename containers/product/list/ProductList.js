import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

// actions
import ProductActions from '../../../reduxs/ProductRedux';

// styles
import { styles } from './styles';

// mui
import withStyles from '@material-ui/core/styles/withStyles';
import { Grid } from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';

// components
import ProductCard from '../../../components/product/list/product-card';
import { NotData } from '../../../components/common';

class ProductList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      classify: {
        product: 'products',
      },
    };
  }

  componentDidMount() {
    this.props.getProduct(this.state.classify.product);
  }

  render() {
    const { product } = this.state.classify;
    const { content, fetching, error, classes } = this.props;

    if (fetching[product]) return <LinearProgress />;

    if (error[product]) return <NotData message={error[product]} />;

    return (
      <div className={classes.wrapper}>
        <Grid container spacing={3} className={classes.container}>
          {content[product]?.map((p) => (
            <Grid item xs={6} sm={4} md={3} key={p.id}>
              <ProductCard {...p} />
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => state.product;

const mapDispatchToProps = (dispatch) => ({
  getProduct: (classify) => dispatch(ProductActions.getProductRequest(classify)),
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ProductList);
