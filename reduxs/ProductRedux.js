import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  getProductRequest: ['classify'],
  getProductSuccess: ['classify', 'payload'],
  getProductFailure: ['classify', 'error'],
});

export const PorductTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  error: {},
  fetching: {},
  content: {},
});

export const getProductRequest = (state, { classify }) => {
  return state.merge({
    fetching: { ...state.fetching, [classify]: true },
    content: { ...state.content, [classify]: null },
    error: { ...state.error, [classify]: null },
  });
};

export const getProductSuccess = (state, { classify, payload }) => {
  return state.merge({
    fetching: { ...state.fetching, [classify]: false },
    content: { ...state.content, [classify]: payload },
  });
};

export const getProductFailure = (state, { classify, error }) => {
  return state.merge({
    fetching: { ...state.fetching, [classify]: false },
    error: { ...state.error, [classify]: error },
  });
};

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PRODUCT_REQUEST]: getProductRequest,
  [Types.GET_PRODUCT_SUCCESS]: getProductSuccess,
  [Types.GET_PRODUCT_FAILURE]: getProductFailure,
});
